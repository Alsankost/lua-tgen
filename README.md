# Lua TGen

A library for generating code from templates for Lua


## Getting started

`tgen.lua` can be used as a library and as a standalone executable.

### As library 

- install library (place to librarys path or add custom path to `package.path`)
- require library to your code `local tgen = require("tgen")`
- prepare a table with data
- call `tgen.gen` with template string and data table
- prepare result

**For example**:

```lua
-- tgen placed to $PWD path
local tgen = require("tgen")

local template = [[
#include <stdio.h>

void foo(int arg1. int arg2) {
	int result = @WORD:expression@;
	printf("%d\n", result);
}

int main(int argc, char* argv[]) {
	@LIST:calls:foo($1, $2);@

	return 0;
}
]]

local data = {
	expression = "arg1 * 2 + arg2",
	calls = {
		{ 1, 50 },
		{ 2, 32 },
		{ 1, 41 }
	}
}

print(tgen.gen(template, data))

--[[
Result:

#include <stdio.h>

void foo(int arg1. int arg2) {
	int result = arg1 * 2 + arg2;
	printf("%d\n", result);
}

int main(int argc, char* argv[]) {
	foo(1, 50);
	foo(2, 32);
	foo(1, 41);

	return 0;
}
]]
```

### As standalone executable

Run this script with wtih table (in string view), path to template file and path to result:

`lua tgen.lua "{ data ... }" <path_to_template_file> <path_to result_file>`

For example:

`lua tgen.lua "{ name = 'this is name', value = 100 }" my_template_file.c result.c`

> currently this approach can execute code from data argument, please DO NOT USE this approach with data from external source!

## Macro

Currently supports 3 macros:

- WORD
- LINES
- LIST

Each macro uses `@` for start and end and looks like:

```
@MACRO_NAME:data_name:args...@
```

### WORD

- **Tepmlate:** `@WORD:name@`
- **Data**: any data

Just a word.
Takes data by key `name` from macro, converts it to a string and inserts it instead of a macro.

**Temlate**:

```c
void @WORD:function@();	
```

**Data**:

```
{
	function = "foo"
}
```

**Result**:

```c
void foo();	
```


### LINES

- **Tepmlate:**
  - `@LINES:name@`
  - `@LINES:name:separator@`
- **Data**: array-like table with simple data

Generating lines from data items. For each line, the text will be copied before and after the macro.

The last argument in the macro can be passed a separator, it will be inserted at the end of every line except the last.

**Temlate**:

```c
enum Status {
	@LINES:status_enum:,@`
};
```

**Data**:

```
{
	status_enum = {
		"STATUS_ERROR",
		"STATUS_NO_DATA",
		"STATUS_OK",
		"STATUS_WAITING"
	}
}
```

**Result**:

```c
enum Status {
	STATUS_ERROR,
	STATUS_NO_DATA,
	STATUS_OK,
	STATUS_WAITING
};
```


### LIST

- **Tepmlate:**
  - `@LIST:name:text_with_args@`
  - `@LIST:name:text_with_args:separator@`
- **Data**: array-like table with array-like tables

Is the same as LINES, but other tables are used as element, values from which can be used in any order.

**Temlate**:

```c
enum Status {
	@LIST:status_enum:$1 = $2:,@`
};
```

**Data**:

```
{
	status_enum = {
		{ "STATUS_ERROR"  , -2 },
		{ "STATUS_NO_DATA", -1 },
		{ "STATUS_OK"     ,  0 },
		{ "STATUS_WAITING",  1 }
	}
}
```

**Result**:

```c
enum Status {
	STATUS_ERROR = -2,
	STATUS_NO_DATA = -1,
	STATUS_OK = 0,
	STATUS_WAITING = 1
};
```
