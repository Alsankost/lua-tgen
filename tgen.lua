local TGen = {}

TGen.macros = {
	--[[
	
	{ --NAME
		patterns = {
			{ "pattern1", count_of_matches_1, math_id_for_dataname_1 },
			{ "pattern2", count_of_matches_2, math_id_for_dataname_2 },
			...
			{ "patternN", count_of_matches_N, math_id_for_dataname_N },
		},
		func = function(data, pattern, ...mathes...)
			...code

			return result
		end
	}

	]]

	{ --WORD
		patterns = {
			{ "@WORD:([%w_]+)@", 1, 1 },
		},
		func = function(data)
			return data
		end
	},

	{ --LINES
		patterns = {
			{ "([^\n]*)@LINES:([%w_]+):([^@:])@([^\n]*)", 4, 2 },
			{ "([^\n]*)@LINES:([%w_]+)()@([^\n]*)",       4, 2 }
		},
		func = function(data, pattern, prefix, _, separator, postfix)
			local result = {}

			for i = 1, #data do
				result[i] = string.format("%s%s%s%s",
					prefix, tostring(data[i]), postfix,
					(pattern == 1 and i ~= #data) and separator or ""
				)
			end

			return table.concat(result, '\n')
		end
	},

	{ --LIST
		patterns = {
			{ "([^\n]*)@LIST:([%w_]+):([^@:]+):([^@:])@([^\n]*)", 5, 2 },
			{ "([^\n]*)@LIST:([%w_]+):([^@:]+)()@([^\n]*)"      , 5, 2 }
		},
		func = function(data, pattern, prefix, _, template, separator, postfix)
			local result = {}

			for i = 1, #data do
				result[i] = string.format("%s%s%s%s",
					prefix,
					template:gsub(
						"$(%d+)",
						function(column)
							if type(data[i]) == "table" then return tostring(data[i][tonumber(column)])
							else return tostring(data[i]) end
						end),
					postfix,
					(pattern == 1 and i ~= #data) and separator or ""
				)
			end

			return table.concat(result, '\n')
		end
	}
}

function TGen.gen(template, data)
	for i = 1, #TGen.macros do
		local cm = TGen.macros[i]

		for i = 1, #cm.patterns do
			local pattern = cm.patterns[i]

			while true do
				local result = { template:match(string.format("%s%s%s", "()", pattern[1], "()")) }
				if not result[1] then break end

				local rdata = data[result[pattern[3] + 1]]
				local mbegin, mend = tonumber(result[1]), tonumber(result[pattern[2] + 2])
				if mbegin == 1 then mbegin = 2 end
				
				if rdata then
					local content = cm.func(rdata, i, table.unpack(result, 2, #result - 1))
					template = template:sub(1, mbegin - 1)..content..template:sub(mend, template:len())
				else
					template = template:sub(1, mbegin - 1)..template:sub(mend, template:len())
				end
			end
		end
	end

	template = template:gsub("__cl__", ":")
	template = template:gsub("__nl__", "\n")
	template = template:gsub("__ml__", "@")

	return template
end

function TGen.genFileToFile(data, templatePath, resultPath)
	local file, err = io.open(templatePath, "r")
	if not file then return nil, err end

	local template = file:read("a*")
	file:close()

	file, err = io.open(resultPath, "w")
	if not file then return nil, err end

	file:write(TGen.gen(template, data))
	file:close()

	return true
end

if arg then
	if arg[1] == "-h" or arg[1] == "--help" then
		print("Usage: lua tgen.lua <data> <template_file> <output_file>")
		os.exit(0)
	
	elseif arg[1] and arg[2] and arg[3] then
		local func, err = load(string.format("return %s", arg[1]))
		if not func then
			print("Invalid data:", err)
			os.exit(1)
		end

		local pass, result = pcall(func)
		if not pass then
			print("Invalid data:", result)
			os.exit(1)
		end


		local pass, err = TGen.genFileToFile(result, arg[2], arg[3])
		if not pass then
			print(err)
		end

		os.exit(0)
	end
end

return TGen
